package com.example.admin.temperature;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    RelativeLayout relativeLayout;
    TextView tv_c, tv_f;

    SeekBar seekBar;

    double c, f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        relativeLayout = (RelativeLayout) findViewById(R.id.layout);

        tv_c = (TextView) findViewById(R.id.tv_c);
        tv_f = (TextView) findViewById(R.id.tv_f);

        seekBar = (SeekBar) findViewById(R.id.seekBar5);
        seekBar.setMax(400);
        seekBar.setProgress(200);

        c = seekBar.getProgress() - 200;
        f = c * 1.8 + 32;
        tv_c.setText(String.format(Locale.getDefault(), "%.1f", c)+" C");
        tv_f.setText(String.format(Locale.getDefault(), "%.1f", f)+" F");

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                c = progress - 200;
                f = c * 1.8 + 32;
                tv_c.setText(String.format(Locale.getDefault(), "%.1f", c)+" C");
                tv_f.setText(String.format(Locale.getDefault(), "%.1f", f)+" F");
                updateBackground();

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void updateBackground() {
        int seekBarValue;
        int correctForRGB = 56;

        seekBarValue = seekBar.getProgress();

        if(seekBarValue == 200) {
            relativeLayout.setBackgroundColor(Color.WHITE);
        }
        if(seekBarValue < 200) {
            int tmp = seekBarValue + correctForRGB;
            relativeLayout.setBackgroundColor(0x0000FF - tmp * 0xFFFFFF);

        }
        if(seekBarValue > 200) {
            seekBarValue = seekBarValue - 200 + correctForRGB;
            relativeLayout.setBackgroundColor(0xff000000 + seekBarValue * 0xFFFFFF);
        }

    }
}
